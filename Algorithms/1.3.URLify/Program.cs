﻿using System;

namespace _1._3.URLify
{
    class Program
    {
        /*
         Write a method to replace all spaces in a string with '%20'. You may assume that the string
         has sufficient space at the end to hold the additional characters, and that you are given the "true"
         length of the string. (Note: If implementing in Java, please use a character array so that you can
         perform this operation in place.)
         EXAMPLE
         Input: "Mr John Smith ", 13
         Output: "Mr%20John%20Smith"
         */
        static void Main(string[] args)
        {
            var input = Console.ReadLine().ToCharArray();
            var trueLength = int.Parse(Console.ReadLine());

            var result = URLify(input, trueLength);
            Console.WriteLine(result);
            
        }

        private static char[] URLify(char[] input, int length)
        {
            //count spaces
            var count = 0;
            for (int i = 0; i < length; i++)
            {
                if (input[i] ==' ')
                {
                    count++;
                }
            }
            //triverse backwards starting from trueLength and URLify spaces using original lengrh
            var index = 2 * count + length;
            for (int i = length - 1; i >=0; i--)
            {
                if (input[i] == ' ')
                {
                    input[index - 1] = '0';
                    input[index - 2] = '2';
                    input[index - 3] = '%';
                    index = index - 3;
                }
                else
                {
                    input[index-1] = input[i];
                    index--;
                }
            }

            return input;
        }
    }
}
