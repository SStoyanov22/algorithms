﻿using System;

namespace _1._8.ZeroMatrix
{
    class Program
    {
        /*
           Write an algorithm such that if an element in an MxN matrix is 0, its entire row and
           column are set to 0.
         */
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var m = int.Parse(Console.ReadLine());
            string[,] matrix = new string[n,m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    var pixel = Convert.ToString(Console.Read());
                    matrix[i,j] = pixel;
                }
            }

            ZeroMatrix(matrix);
            PrintMatrix(matrix);
        }

        private static void PrintMatrix(string[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i,j]);
                }
                Console.WriteLine();
            }
        }

        private static void ZeroMatrix(string[,] matrix)
        {
            var zeroRows = new bool[matrix.GetLength(0)];
            var zeroColumns = new bool[matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i,j] == "0")
                    {
                        zeroRows[i] = true;
                        zeroColumns[j] = true;
                    }
                }
            }

            for (int i = 0; i < zeroRows.Length; i++)
            {
                if (zeroRows[i])
                {
                    NullifyRow(matrix,i);   
                }
            }

            for (int j = 0; j < zeroColumns.Length; j++)
            {
                if (zeroColumns[j])
                {
                    NullifyColumn(matrix, j);
                }
            }
        }

        private static void NullifyColumn(string[,] matrix, int j)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                matrix[i, j] = "0";
            }
        }

        private static void NullifyRow(string[,] matrix, int i)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                matrix[i, j] = "0";
            }
        }

    }
}
