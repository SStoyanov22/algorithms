﻿using System;

namespace _1._1.IsUnique
{
    class Program
    {
        /*
         Implement an algorithm to determine if a string has all unique characters. What if you
         cannot use additional data structures?
        */
        static void Main(string[] args)
        {
            var input = Console.ReadLine();
            var result = IsUniqueChars(input);
            Console.WriteLine(result);
        }

        public static bool IsUniqueChars(string input)
        {
            //ascii table has 128 characters
            if (input.Length>128)
            {
                return false;
            }

            var chars = new bool[128];

            for (int i = 0; i < input.Length - 1; i++)
            {
                var position = input[i] - 96;
                if (chars[position])
                {
                    return false;
                }

                chars[position] = true;
            }

            return true;
        }
    }
}
