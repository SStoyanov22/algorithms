﻿using System;
using System.Collections.Generic;

namespace _1._2.CheckPermutation
{
    class Program
    {
        /*
            Given two strings, write a method to decide if one is a permutation of the
            other.
         */
        static void Main(string[] args)
        {
            var str1 = Console.ReadLine();
            var str2 = Console.ReadLine();

            var result = CheckPermutation(str1, str2);
            Console.WriteLine(result);
        }

        private static bool CheckPermutation(string str1, string str2)
        {
            //compare lengths
            if (str1.Length != str2.Length)
            {
                return false;
            }
            var countChars = new int[128];
            //count chars in first string
            for (int i = 0; i < str1.Length; i++)
            {
                var pos1 = str1[i] - 96;
                countChars[pos1]++;
            }

            //count chars in second string by substracting counts of first
            for (int i = 0; i < str2.Length; i++)
            {
                var pos2 = str2[i] - 96;
                countChars[pos2]--;
                if (countChars[pos2]<0)
                {
                    return false;   
                }
            }

            return true;
        }
    }
}
