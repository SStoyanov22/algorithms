﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _1._6.StringCompression
{
    class Program
    {
        /*
          Implement a method to perform basic string compression using the counts
          of repeated characters. For example, the string aabcccccaaa would become a2blc5a3. If the
          "compressed" string would not become smaller than the original string, your method should return
          the original string. You can assume the string has only uppercase and lowercase letters (a - z).
        */
        static void Main(string[] args)
        {
            var input = Console.ReadLine();

            var compressedInput = StringCompress(input);
            Console.WriteLine(compressedInput);
        }

        private static string StringCompress(string input)
        {
            //We use string builder because it is much faster to concatanate strings at the end
            var result = new StringBuilder();

            var currentCount = 0;

            for (int i = 0; i < input.Length; i++)
            {
                currentCount++;

                if (i+1 >= input.Length || input[i] != input[i+1])
                {
                    result.Append(input[i]);
                    result.Append(currentCount);
                    currentCount = 0;
                }
            }

            return result.Length >= input.Length ? input : result.ToString();
        }

    }
}
