﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day_17
{
    class Program
    {
        static void Main(string[] args)
        {
            /*---Day 17: Conway Cubes ---*/

           var input = System.IO.File
                .ReadAllLines(@"C:\My stuff\gitlab\algorithms\Algorithms\AdventOfCode\Day17\Day_17\input.txt");

            /*--PartOne--*/
            Dictionary<(int x, int y, int z), int> count3d;
            Dictionary<(int x, int y, int z), bool> cubes3d;
            (int x, int y, int z)[] directions3d;
            Init3d(input, out count3d, out cubes3d, out directions3d);
            Solve3d(count3d, cubes3d, directions3d);

            /*--PartTwo--*/
            Dictionary<(int x, int y, int z, int w), int> count4d;
            Dictionary<(int x, int y, int z , int w), bool> cubes4d;
            (int x, int y, int z, int w)[] directions4d;
            Init4d(input, out count4d, out cubes4d, out directions4d);
            Solve4d(count4d, cubes4d, directions4d);

        }

        private static void Solve3d(Dictionary<(int x, int y, int z), int> count3d, Dictionary<(int x, int y, int z), bool> cubes3d, (int x, int y, int z)[] directions3d)
        {
            var numberOfCycles = 6;
            for (int i = 0; i < numberOfCycles; i++)
            {
                count3d.Clear();
                //restart all counts on start of new cycle
                foreach (var p in cubes3d.Keys)
                {
                    count3d[p] = 0;
                }

                //increase count for neighbour if active
                foreach (var ((x, y, z), isActive) in cubes3d.Where(p => p.Value))
                {
                    if (isActive)
                        foreach (var (dx, dy, dz) in directions3d)
                        {
                            count3d[(x + dx, y + dy, z + dz)] = count3d.GetValueOrDefault((x + dx, y + dy, z + dz)) + 1;
                        }
                }

                //change state depending on ruling
                foreach (var (pos, activeNeighbours) in count3d)
                {
                    var active = cubes3d.GetValueOrDefault(pos);
                    if (active && (activeNeighbours == 2 || activeNeighbours == 3))
                    {
                        cubes3d[pos] = true;
                    }
                    else if (!active && activeNeighbours == 3)
                    {
                        cubes3d[pos] = true;
                    }
                    else
                    {
                        cubes3d[pos] = false;
                    }
                }
                Console.WriteLine(cubes3d.Where(kv => kv.Value).Count(v => v.Value == true).ToString());

            }
        }

        private static void Init3d(string[] input, out Dictionary<(int x, int y, int z), int> count3d, out Dictionary<(int x, int y, int z), bool> cubes3d, out (int x, int y, int z)[] directions3d)
        {
            count3d = new Dictionary<(int x, int y, int z), int>(64);
            count3d.Clear();
            cubes3d = new Dictionary<(int x, int y, int z), bool>(64);
            directions3d = Enumerable.Range(-1, 3)
                .SelectMany(x => Enumerable.Range(-1, 3)
                    .SelectMany(y => Enumerable.Range(-1, 3)
                        .Select(z => (x, y, z))))
                .Where(d => d != (0, 0, 0))
                .ToArray();

            //initiate cubes matrix
            var size = input.Length;
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    for (int z = -1; z < size - 1; z++)
                    {
                        if (z != 0)
                        {
                            cubes3d[(x, y, z)] = false;
                        }
                        else
                        {
                            var isActive = input[x][y] == '#';
                            cubes3d[(x, y, z)] = isActive;
                        }
                    }

                }
            }
        }

        private static void Solve4d(Dictionary<(int x, int y, int z, int w), int> count4d, Dictionary<(int x, int y, int z, int w), bool> cubes4d, (int x, int y, int z, int w)[] directions4d)
        {
            var numberOfCycles = 6;
            for (int i = 0; i < numberOfCycles; i++)
            {
                count4d.Clear();
                //restart all counts on start of new cycle
                foreach (var p in cubes4d.Keys)
                {
                    count4d[p] = 0;
                }

                //increase count for neighbour if active
                foreach (var ((x, y, z, w), isActive) in cubes4d.Where(p => p.Value))
                {
                    if (isActive)
                        foreach (var (dx, dy, dz, dw) in directions4d)
                        {
                            count4d[(x + dx, y + dy, z + dz, w + dw)] = count4d.GetValueOrDefault((x + dx, y + dy, z + dz, w + dw)) + 1;
                        }
                }

                //change state depending on ruling
                foreach (var (pos, activeNeighbours) in count4d)
                {
                    var active = cubes4d.GetValueOrDefault(pos);
                    if (active && (activeNeighbours == 2 || activeNeighbours == 3))
                    {
                        cubes4d[pos] = true;
                    }
                    else if (!active && activeNeighbours == 3)
                    {
                        cubes4d[pos] = true;
                    }
                    else
                    {
                        cubes4d[pos] = false;
                    }
                }
                Console.WriteLine(cubes4d.Where(kv => kv.Value).Count(v => v.Value == true).ToString());

            }
        }

        private static void Init4d(string[] input, out Dictionary<(int x, int y, int z, int w), int> count4d, out Dictionary<(int x, int y, int z, int w), bool> cubes4d, out (int x, int y, int z, int w)[] directions4d)
        {
            count4d = new Dictionary<(int x, int y, int z, int w), int>(1024);
            count4d.Clear();
            cubes4d = new Dictionary<(int x, int y, int z, int w), bool>(1024);
            directions4d = Enumerable.Range(-1, 3)
                .SelectMany(x => Enumerable.Range(-1, 3)
                    .SelectMany(y => Enumerable.Range(-1, 3)
                        .SelectMany(z => Enumerable.Range(-1, 3)
                        .Select(w => (x, y, z, w)))))
                .Where(d => d != (0, 0, 0, 0))
                .ToArray();

            //initiate cubes matrix
            var size = input.Length;
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    for (int z = -1; z < size - 1; z++)
                    {
                        for (int w = -1; w < size -1; w++)
                        {
                            if (z == 0 && w == 0)
                            {
                                var isActive = input[x][y] == '#';
                                cubes4d[(x, y, z, w)] = isActive;
                            }
                            else
                            {
                                cubes4d[(x, y, z, w)] = false;
                            }
                        }
                    }

                }
            }
        }

    }
}
