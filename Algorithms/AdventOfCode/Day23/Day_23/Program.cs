﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllText(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day23\Day_23\input.txt")
                .ToCharArray()
                .Select(x => (int)char.GetNumericValue(x))
                .ToArray();

            var cupsPartOne = input.Zip(input.Skip(1).Append(input.First()))
                .ToDictionary(t => t.Item1, t => t.Item2);
            Move(cupsPartOne,100);
            var answerPartOne = Print(cupsPartOne);
            Console.WriteLine(answerPartOne);

            var input2 = input.Concat(Enumerable.Range(input.Max() + 1, 1_000_000 - input.Count()));
            var cupsPartTwo = input2.Zip(input2.Skip(1).Append(input.First())).ToDictionary(t => t.Item1, t => t.Item2);
            Move(cupsPartTwo, 10000000);
            var answerPartTwo = (long)cupsPartTwo[1] * (long)cupsPartTwo[cupsPartTwo[1]];
            Console.WriteLine(answerPartTwo);
        }

        private static void Move(Dictionary<int, int> cups, int times)
        {
            var current = cups.Keys.First();
            var max = cups.Keys.Max();
            var threeCups = new int[3];

            for (int i = 0; i < times; i++)
            {
                threeCups[0] = cups[current];
                threeCups[1] = cups[threeCups[0]];
                threeCups[2] = cups[threeCups[1]];

                cups[current] = cups[threeCups[2]];

                var destination = current - 1;
                while (true)
                {
                    if (threeCups.Contains(destination))
                    {
                        destination--;
                        continue;
                    }
                    if (destination < 1)
                    {
                        destination = max;
                        continue;
                    }
                    break;
                }

                var tmp = cups[destination];
                cups[destination] = threeCups[0];
                cups[threeCups[2]] = tmp;

                current = cups[current];
            }
        }

        public static string Print(Dictionary<int, int> cups)
        {
            var sb = new StringBuilder();
            var i = 1;

            while (cups[i] != 1)
            {
                sb.Append(cups[i]);
                i = cups[i];
            }

            return sb.ToString();
        }
    }
}
