﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_16
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllText(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day16\Day_16\input.txt");
            string[] segments = input.Split(new string[] { Environment.NewLine + Environment.NewLine },
                               StringSplitOptions.RemoveEmptyEntries).ToArray();

            var rulesDescriptions = segments[0];
            var myTicket = segments[1].Split("\r\n")
                                    .Last()
                                    .Split(',')
                                    .Select(x => int.Parse(x))
                                    .ToArray();
            var otherTickets = segments[2].Split("\r\n").Skip(1).Select(l => l.Split(',').Select(int.Parse).ToArray()).ToArray();
            var rulz = rulesDescriptions.Split("\r\n")
                                        .Select(l => (
                                                      name: l[..l.IndexOf(':')],
                                                      values: Regex.Matches(l, @"(\d+)-(\d+)").OfType<Match>()
                                                                    .Select(m => (lo: int.Parse(m.Groups[1].Value), hi: int.Parse(m.Groups[2].Value)))
                                                                    .ToArray()))
                                                            .ToArray();


            var validNumbers = rulz.SelectMany(x => x.values).ToArray();
            var answerPartOne = otherTickets
                .SelectMany(t => t)
                .Where(n => !validNumbers.Any(x => Enumerable.Range(x.lo, x.hi - x.lo + 1).Contains(n)))
                .Sum()
                .ToString();
            Console.WriteLine(answerPartOne);


            var validTickets = otherTickets
                .Where(t => t.All(n => validNumbers.Any(x => Enumerable.Range(x.lo, x.hi - x.lo +1).Contains(n))))
                .ToArray();

            var candidates = rulz
                .Select(r => (
                    r.name,
                    indexes: Enumerable.Range(0, validTickets[0].Length)
                        .Where(i => validTickets.All(t => r.values.Any(v => Enumerable.Range(v.lo,v.hi - v.lo + 1).Contains(t[i]))))
                        .ToList()))
                .ToList();

            var map = new List<(string name, int index)>();
            while (map.Count < rulz.Length)
            {
                var nextCandidate = candidates.First(c => c.indexes.Count == 1);
                var index = nextCandidate.indexes[0];
                map.Add((nextCandidate.name, index));
                candidates.RemoveAll(c => c.name == nextCandidate.name);
                foreach (var (_, indexes) in candidates)
                    indexes.Remove(index);
            }

            var answerPartTwo = map
                .Where(r => r.name.StartsWith("departure"))
                .Select(r => myTicket[r.index])
                .Aggregate(1L, (l, r) => l * r)
                .ToString();

            Console.WriteLine(answerPartTwo);
        }
    }
}
