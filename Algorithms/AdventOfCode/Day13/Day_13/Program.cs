﻿using System;
using System.Linq;

namespace Day_13
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllLines(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day13\Day_13\input.txt");


            var times = input[1].Split(',');

            var myEarliestTime = int.Parse(input[0]);

            var answerPartOne = times
                .Where(t => t != "x")
                .Select(t => int.Parse(t))
                .Select(b => (busN: b,
                              firstTimeAfter: (myEarliestTime / b + 1) * b - myEarliestTime))
                .OrderBy(b => b.firstTimeAfter)
                .Select(b => b.busN * b.firstTimeAfter)
                .First().ToString();
            Console.WriteLine(answerPartOne);

            var earliestTime = long.Parse(times[0]);
            var increment = earliestTime;
            for (int i = 1; i < times.Length; i++)
            {
                if (times[i] == "x") continue;

                var curTime = long.Parse(times[i]);
                var modValue = curTime - (i % curTime);
                while (earliestTime % curTime != modValue)
                    earliestTime += increment;
                increment = LowestCommonMultiple(increment, curTime);
            }

            var PartB = earliestTime.ToString();
            Console.WriteLine(PartB);

        }

        static long GreatestCommonFactor(long a, long b)
        {
            while (b != 0)
            {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

        static long LowestCommonMultiple(long a, long b)
        {
            return (a / GreatestCommonFactor(a, b)) * b;
        }
    }
}
