﻿using System.Collections.Generic;

namespace Day_14
{
    public class DockingProgramCommand
    {
        public DockingProgramCommand()
        {
            this.ValueToMemoryAddress = new long[2];
        }
        public string Mask { get; set; }

        public  long[]  ValueToMemoryAddress    { get; set; }
    }
}