﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text.RegularExpressions;

namespace Day_14
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllLines(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day14\Day_14\input.txt");

            var regex = new Regex("^(mask = (?<mask>[01X]{36})|(mem\\[(?<memloc>\\d+)\\]) = (?<memval>\\d+))$");
            var matches = input
                .Select(x => regex.Match(x))
                .ToArray();

            
            var answerPartOne = SolvePartOne(matches);
            Console.WriteLine(answerPartOne);

            //var answerPartTwo = SolvePartTwo(matches);
            //Console.WriteLine(answerPartTwo);



        }

        private static string SolvePartOne(Match[] matches)
        {
            var memory = new Dictionary<int, ulong>();
            var mask = (and: ulong.MaxValue, or: ulong.MinValue);

            foreach (var match in matches)
            {
                if (match.Groups["mask"].Success)
                {
                    var s = match.Groups["mask"].Value;
                    mask = (ulong.MaxValue, ulong.MinValue);

                    for (int i = 0; i < s.Length; i++)
                    {
                        if (s[i] == '0')
                            mask.and &= ~(1ul << (35 - i));
                        else if (s[i] == '1')
                            mask.or |= 1ul << (35 - i);
                    }
                }
                else
                {
                    var memloc = int.Parse(match.Groups["memloc"].Value);
                    var memval = ulong.Parse(match.Groups["memval"].Value);
                    memory[memloc] = (memval & mask.and) | mask.or;
                }
            }

            return memory.Sum(kvp => (long)kvp.Value).ToString();
        }

        //private static string SolvePartTwo(Match[] matches)
        //{
        //    var memory = new Dictionary<ulong, ulong>();
        //    var mask = (fl: ulong.MinValue, or: ulong.MinValue);
        //    foreach (var m in matches)
        //    {
        //        if (m.Groups["mask"].Success)
        //        {
        //            var s = m.Groups["mask"].Value;
        //            mask = (ulong.MinValue, ulong.MinValue);
        //            for (int i = 0; i < s.Length; i++)
        //            {
        //                if (s[i] == 'X')
        //                    mask.fl |= 1ul << (35 - i);
        //                else if (s[i] == '1')
        //                    mask.or |= 1ul << (35 - i);
        //            }
        //        }
        //        else
        //        {
        //            static IEnumerable<ulong> getValues(ulong baseValue, ulong fl)
        //            {
        //                var lowest = Bmi1.X64.ExtractLowestSetBit(fl);
        //                if (lowest == 0) return MoreEnumerable.Return(baseValue);
        //                fl &= ~lowest;
        //                return getValues(baseValue, fl).Concat(
        //                    getValues(baseValue | lowest, fl));
        //            }

        //            var baseLocation = (ulong.Parse(m.Groups["memloc"].Value) | mask.or) & ~mask.fl;
        //            var value = ulong.Parse(m.Groups["memval"].Value);
        //            foreach (var v in getValues(baseLocation, mask.fl))
        //                memory[v] = value;
        //        }
        //    }

        //    return memory.Sum(kvp => (long)kvp.Value).ToString();
        //}
    }
}
