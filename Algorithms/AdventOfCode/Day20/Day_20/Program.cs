﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day_20
{
    class Program
    {
        static void Main(string[] args)
        {
			var input = System.IO.File
			  .ReadAllLines(@"C:\My stuff\gitlab\algorithms\Algorithms\AdventOfCode\Day19\Day_19\input.txt");

			//var segments = input.GetLines(StringSplitOptions.None)
			//	.Segment(string.IsNullOrWhiteSpace)
			//	.ToArray();

			var rulesBase = input
				.Select(x => x.Split(':'))
				.ToDictionary(x => x[0], x => x[1]);
			var processed = new Dictionary<string, string>();

			

		}

		public static string BuildRegex(string input, Dictionary<string, string> processed, Dictionary<string,string> rulesBase)
		{
			if (processed.TryGetValue(input, out var s))
				return s;

			var orig = rulesBase[input];
			if (orig.StartsWith('\"'))
				return processed[input] = orig.Replace("\"", "");

			//if (!orig.Contains("|"))
			//	return processed[input] = string.Join("", orig.Split().Select(BuildRegex));

			return processed[input] =
				"(" +
				string.Join("", orig.Split().Select(x => x == "|" ? x : BuildRegex(x,processed,rulesBase))) +
				")";
		}
	}
}
