﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_15
{
    public class SolvePartOne : ISolve
    {
        public SolvePartOne(string[] input)
        {
            this.Numbers = ProcessInput.From(input);
            this.Last = int.Parse(input[input.Length - 1]);
            this.Turn = input.Length;
        }
        public Dictionary<int,List<int>> Numbers {get; set;}

        public int Turn { get; set; }

        public int Last { get; set; }

        public void Solve(int turn)
        {
            while (this.Turn < turn)
            {
                this.Turn++;

                if (this.Numbers[this.Last].Count == 1)
                {
                    AddTurnToNumber(0);
                }
                else
                {
                    var lastNumberIndexCount = this.Numbers[this.Last].Count;
                    var lastTurn = this.Numbers[this.Last][lastNumberIndexCount - 1];
                    var preLastTurn = this.Numbers[this.Last][lastNumberIndexCount - 2];
                    var newNumber = lastTurn - preLastTurn;
                    
                    AddTurnToNumber(newNumber);
                }
            }

        }

        private void AddTurnToNumber(int newNumber)
        {
            if (!this.Numbers.ContainsKey(newNumber))
            {
                this.Numbers[newNumber] = new List<int>();
            }

            this.Last = newNumber;
            this.Numbers[newNumber].Add(this.Turn);
        }
    }
}
