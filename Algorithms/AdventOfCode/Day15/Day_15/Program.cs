﻿using System;

namespace Day_15
{
    public class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllText(@"C:\My stuff\gitlab\algorithms\Algorithms\AdventOfCode\Day15\Day_15\input.txt").Split(",");
            

            /*--PartOne--*/
            var solvePartOne = new SolvePartOne(input);
             solvePartOne.Solve(2020);
            Console.WriteLine(solvePartOne.Last);

            /*--PartTwo--*/
            var solvePartTwo = new SolvePartOne(input);
            solvePartTwo.Solve(30000000);
            Console.WriteLine(solvePartTwo.Last);
        }
    }
}
