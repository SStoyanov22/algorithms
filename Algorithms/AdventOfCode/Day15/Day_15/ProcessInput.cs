﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Day_15
{
    public class ProcessInput
    {
        public static Dictionary<int,List<int>> From(string[] input)
        {
            var numbersMap = new Dictionary<int,List<int>>();
            var turn = 0;

            foreach (var item in input)
            {
                turn++;
                var num = int.Parse(item);
                if (!numbersMap.ContainsKey(num))
                {
                    numbersMap[num] = new List<int>();
                }

                numbersMap[num].Add(turn);
            }

            return numbersMap;
        }
    }
}
