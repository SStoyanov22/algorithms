﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_15
{
    public interface ISolve
    {
        Dictionary<int, List<int>> Numbers { get; }
        void Solve(int turn);
        
        int Turn { get; }
        int Last { get; }
    }
}
