﻿using System;
using System.Linq;

namespace Day_11
{
    class Program
    {
        private static readonly (int dy, int dx)[] directions = { 
			(-1, -1), 
			(-1, 0), 
			(-1, 1),
			(0, -1), 
			(0, 1), 
			(1, -1), 
			(1, 0), 
			(1, 1), };
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllLines(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day11\Day_11\input.txt");
			var map = input.Select(c => c.ToCharArray()).ToArray();

			//var PartA = RunPart(map, true);
   //         Console.WriteLine(PartA);
            var PartB = RunPart(map, false);
            Console.WriteLine(PartB);

        }


        private static string RunPart(char[][] map, bool immediatelyAdjacent)
        {
			var count = 0;
            while (true)
            {
                count++;
                var nextMap = RunStep(map, immediatelyAdjacent);
                PrintRound(count, nextMap);
                if (HasSeatsChanged(map, nextMap))
                    break;

                map = nextMap.Select(x => x.ToArray()).ToArray();
            }

            return map.SelectMany(x => x).Count(x => x == '#').ToString();
        }


        private static bool HasSeatsChanged(char[][] map, char[][] nextmap)
        {
            for (int i = 0; i < map.Length; i++)
            {
                for (int j = 0; j < map[i].Length; j++)
                {
                    if (map[i][j] != nextmap[i][j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static void PrintRound(int count, char[][] nextMap)
        {
            Console.WriteLine("Round " + count.ToString());
            foreach (var line in nextMap)
            {
                Console.WriteLine(new string(line));
            }
            Console.WriteLine("---");
        }

        private static char[][] RunStep(char[][] map, bool immediatelyAdjacent)
		{
			var newMap = map.Select(a => a.ToArray()).ToArray();

			for (int y = 0; y < map.Length; y ++)
				for (int x = 0; x < map[y].Length; x++)
                {
                    if (map[y][x] == '.')
                    {
                        newMap[y][x] = '.';
                        continue;
                    }

                   int cnt = ApplyRules(map, y, x, immediatelyAdjacent);
                    var seat = map[y][x];
                    switch (seat, cnt, immediatelyAdjacent)
                    {
                        case ('L', 0, _):
                            newMap[y][x] = '#';
                            break;
                        case ('#', >= 4, true):
                            newMap[y][x] = 'L';
                            break;
                        case ('#', >= 5, false):
                            newMap[y][x] = 'L';
                            break;
                        case (var c, _, _):
                            newMap[y][x] = c;
                            break;
                    };
                }

            return newMap;
		}

        private static int ApplyRules(char[][] map, int y, int x, bool immediatelyAdjacent)
        {
            var cnt = 0;
            //check (immediately?) adjacent seats
            foreach (var (dy, dx) in directions)
            {
                int _y = y, _x = x;

                if (IsOccupied(_y, _x, dy, dx, map, immediatelyAdjacent))
                {
                    cnt++;
                }
            }

            return cnt;
        }

        private static bool IsOccupied(int y, int x, int dy, int dx, char[][] map, bool immediatelyAdjacent)
        {
            y += dy;
            if (y < 0 || y > map.Length - 1)
                return false;
            x += dx;
            if (x < 0 || x > map[y].Length - 1)
                return false;
            if (map[y][x] == 'L')
                return false;
            else if (map[y][x] == '#')
                return true;
            else if (map[y][x] == '.' && !immediatelyAdjacent)
                return IsOccupied(y, x, dy, dx, map, immediatelyAdjacent);
            else
                return false;
        }
    }
}
