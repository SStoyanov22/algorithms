﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_18
{
    class Program
    {
        static void Main(string[] args)
        {
            /*--- Day 18: Operation Order ---*/
            var input = System.IO.File
                .ReadAllLines(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day18\Day_18\input.txt");

            var answerPartOne = input.Select(x => SolvePartOne(x)).Sum();
            Console.WriteLine(answerPartOne);

            var answerPartTwo = input.Select(x => SolvePartTwo(x)).Sum();
            Console.WriteLine(answerPartTwo);
        }

        public static long SolvePartOne(string expression)
        {
            // Handle all nested parentheses
            var innerMatches = Regex.Matches(expression, @"\([\d+* ]+?\)");
            if (innerMatches.Any())
            {
                // Recursive case
                foreach (Match match in innerMatches)
                {
                    var inner = match.Value.Replace("(", "").Replace(")", "");
                    expression = expression.Replace(match.Value, SolvePartOne(inner).ToString());
                }

                return SolvePartOne(expression);
            }
            else
            {
                // Base case
                var operation = '+';
                long result = 0;

                foreach (var item in Regex.Split(expression, @"( \+|\* )"))
                {
                    switch (item.Trim())
                    {
                        case "+":
                            operation = '+';
                            break;
                        case "*":
                            operation = '*';
                            break;
                        default:
                            switch (operation)
                            {
                                case '+':
                                    result += Convert.ToInt64(item);
                                    break;
                                case '*':
                                    result *= Convert.ToInt64(item);
                                    break;
                            }
                            break;
                    }
                }

                return result;
            }
        }

        public static long SolvePartTwo(string expression)
        {
            // Handle all nested parentheses
            var innerMatches = Regex.Matches(expression, @"\([\d+* ]+?\)");
            if (innerMatches.Any())
            {
                // Recursive case
                foreach (Match match in innerMatches)
                {
                    var inner = match.Value.Replace("(", "").Replace(")", "");
                    expression = expression.Replace(match.Value, SolvePartTwo(inner).ToString());
                }

                return SolvePartTwo(expression);
            }
            else
            {
                // Base case
                // Do all the additions first
                while (expression.Contains("+"))
                {
                    var addition = Regex.Match(expression, @"(\d+) \+ (\d+)");
                    var operand1 = Convert.ToInt64(addition.Groups[1].Value);
                    var operand2 = Convert.ToInt64(addition.Groups[2].Value);
                    var sum = operand1 + operand2;
                    expression = new Regex(@"\d+ \+ \d+").Replace(expression, sum.ToString(), 1);
                }

                // Do the multiplications
                return Regex.Split(expression, @" \* ")
                    .Select(long.Parse)
                    .Aggregate((a, b) => a * b);
            }
        }
    }
}
