﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day_12
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllLines(@"C:\My stuff\gitlab\algorithms\Algorithms\AdventOfCode\Day12\Day_12\input.txt")
                .Select(s =>
                        new NavigationInstruction()
                        {
                            Action = s[0],
                            Value = int.Parse(s.Substring(1))
                        })
                .ToList(); ;


            /*--PartOne--*/
            var answerPartOne = SolvePartOne(input);
            Console.WriteLine(answerPartOne);

            ///*--PartTwo--*/
            var answerParttwo = SolvePartTwo(input);
            Console.WriteLine(answerParttwo);
        }

		private static string SolvePartOne(List<NavigationInstruction> instructions)
		{

			int x = 0, y = 0, d = 0;
			foreach (var instruction in instructions)
            {
                ExecuteInstruction(ref x, ref y, ref d, instruction);
            }

            return (Math.Abs(x) + Math.Abs(y)).ToString();
		}

        private static void ExecuteInstruction(ref int x, ref int y, ref int d, NavigationInstruction instruction)
        {
            (x, y, d) = instruction.Action switch
            {
                'F' => Move(x, y, d, d, instruction.Value),

                'E' => Move(x, y, d, 0, instruction.Value),
                'S' => Move(x, y, d, 1, instruction.Value),
                'W' => Move(x, y, d, 2, instruction.Value),                'N' => Move(x, y, d, 3, instruction.Value),

                'R' => RotateDir(x, y, d, instruction.Value / 90),
                'L' => RotateDir(x, y, d, 4 - (instruction.Value / 90)),
            };
        }

        private static (int x, int y, int d) RotateDir(int x, int y, int d, int a) =>
                        (x, y, (d + a) % 4);

        private static (int x, int y, int d) Move(int x, int y, int d, int dir, int amount) =>
                        (x, y, d) = dir switch
                        {
                            0 => (x + amount, y, d),
                            1 => (x, y - amount, d),
                            2 => (x - amount, y, d),
                            3 => (x, y + amount, d),
                        };

        private static string SolvePartTwo(List<NavigationInstruction> instructions)
        {

            int wayx = 10, wayy = 1;
            int x = 0, y = 0;
            foreach (var instruction in instructions)
            {
                (x, y, wayx, wayy) = instruction.Action switch
                {
                    'F' => (x + (wayx * instruction.Value), y + (wayy * instruction.Value), wayx, wayy),

                    'E' => (x, y, wayx + instruction.Value, wayy),
                    'S' => (x, y, wayx, wayy - instruction.Value),
                    'W' => (x, y, wayx - instruction.Value, wayy),
                    'N' => (x, y, wayx, wayy + instruction.Value),

                    'R' => RotateWaypoint(x, y, wayx, wayy, instruction.Value / 90),
                    'L' => RotateWaypoint(x, y, wayx, wayy, 4 - (instruction.Value / 90)),
                };
            }

            return (Math.Abs(x) + Math.Abs(y)).ToString();
        }

        private static (int x, int y, int wayx, int wayy) RotateWaypoint(int x, int y, int wayx, int wayy, int a) =>
                        a switch
                        {
                            0 => (x, y, wayx, wayy),
                            1 => (x, y, wayy, -wayx),
                            2 => (x, y, -wayx, -wayy),
                            3 => (x, y, -wayy, wayx),
                        };
    }
}
