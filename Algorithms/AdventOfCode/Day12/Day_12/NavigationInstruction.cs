﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_12
{
    public class NavigationInstruction
    {
        public char Action { get; set; }
        public int Value { get; set; }
    }
}
