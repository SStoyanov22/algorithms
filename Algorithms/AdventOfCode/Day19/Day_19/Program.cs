﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_19
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day19\Day_19\input.txt");
            var parts = input.Split(new string[] { "\r\n" },
                               StringSplitOptions.RemoveEmptyEntries);
            var rules = parts.Where(l => l.Contains(": "))
                .Select(x => x.Split(": "))
                .ToDictionary(x => x[0], x => x[1]);
            var messages = parts.Where(l => !l.Contains(":")).ToArray();
            var processedRules = new Dictionary<string, string>();

            

            var regex = new Regex("^" + BuildRegex("0",processedRules,rules) + "$");
            var answerPartOne= messages.Count(regex.IsMatch).ToString();
            Console.WriteLine(answerPartOne);

            regex = new Regex($@"^({BuildRegex("42",processedRules,rules)})+(?<open>{BuildRegex("42",processedRules,rules)})+(?<close-open>{BuildRegex("31",processedRules,rules)})+(?(open)(?!))$");
            var answerPartTwo = messages.Count(regex.IsMatch).ToString();
            Console.WriteLine(answerPartTwo);
        }

        static string BuildRegex(string input, Dictionary<string,string> processedRules, Dictionary<string,string> rules)
        {
            if (processedRules.TryGetValue(input, out var s))
                return s;

            var orig = rules[input];
            if (orig.StartsWith('\"'))
                return processedRules[input] = orig.Replace("\"", "");

            if (!orig.Contains("|"))
                return processedRules[input] = string.Join("", orig.Split(" ").Select(x => BuildRegex(x,processedRules,rules)));

            return processedRules[input] =
                "(" +
                string.Join("", orig.Split().Select(x => x == "|" ? x : BuildRegex(x,processedRules,rules))) +
                ")";
        }


    }
}
