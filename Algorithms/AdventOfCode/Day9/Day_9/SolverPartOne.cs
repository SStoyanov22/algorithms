﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day_9
{
    public class SolverPartOne : ISolve
    {
        public SolverPartOne(IList<long> processedInput)
        {
            this.ProcessedInput = processedInput;
        }

        public IList<long> ProcessedInput { get; set; }
        public long Solve()
        {
            return this.ProcessedInput.FirstOrDefault(x => !IsValid(x));
        }

        private bool IsValid(long number)
        {
            var range = 25;
            var index = this.ProcessedInput.IndexOf(number);
            if (index == -1)
            {
                throw new KeyNotFoundException();
            }

            if (index < range)
            {
                return true;
            }
            else
            {
                int startIndex = index - range;

                for (int i = startIndex; i < index - 1; i++)
                {
                    for (int j = startIndex + 1; j < index; j++)
                    {
                        if (this.ProcessedInput[i] + this.ProcessedInput[j] == this.ProcessedInput[index])
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }
}
