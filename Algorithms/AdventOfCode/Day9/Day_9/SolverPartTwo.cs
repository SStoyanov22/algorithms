﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_9
{
    public class SolverPartTwo : ISolve
    {
        private long answer;
        public SolverPartTwo(IList<long> processedInput, long partOneAnswer)
        {
            this.ProcessedInput = processedInput;
            this.answer = partOneAnswer;
        }

        public IList<long> ProcessedInput { get; set; }

        public long Solve()
        {
            return FindEncryptionWeakness();
        }

        private long FindEncryptionWeakness()
        {
            long result = 0;
            for (int i = 0; i < this.ProcessedInput.Count - 1; i++)
            {
                result = this.ProcessedInput[i];
                long smallest = this.ProcessedInput[i];
                long largest = this.ProcessedInput[i];

                for (int j = i+1; j < this.ProcessedInput.Count; j++)
                {
                    result += this.ProcessedInput[j];

                    if (result > this.answer)
                    {
                        break;
                    }

                    if (this.ProcessedInput[j] < smallest)
                    {
                        smallest = this.ProcessedInput[j];
                    }

                    if (this.ProcessedInput[j] > largest)
                    {
                        largest = this.ProcessedInput[j];
                    }

                    if (result == this.answer)
                    {
                        return smallest + largest;
                    }

                }
            }

            return result;
        }
    }
}
