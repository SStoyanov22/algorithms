﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day_9
{
    public class ProcessInput
    {
        public static List<long> From(string input) => input.Split("\r\n")
                .Select(s => long.Parse(s))
                .ToList();
    }
}
