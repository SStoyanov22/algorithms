﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_9
{
    public interface ISolve
    {
        IList<long> ProcessedInput { get; }
        long Solve();
    }
}
