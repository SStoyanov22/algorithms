﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_4
{
    class Program
    {
		/*
         * 
         */
		static void Main(string[] args)
		{
			var input = System.IO.File.ReadAllText(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day4\Day_4\input.txt");
			var required = new[] { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", };

            //get input into data structure
            var passports = input.Split("\r\n\r\n")
            .Select(l => l.Split(new[] { "\r\n", " " },StringSplitOptions.RemoveEmptyEntries)
                .Select(s => s.Split(':'))
                .ToDictionary(
                    a => a[0],
                    a => a[1]));

            //count number of valid passports that have all required fields
            var PartA = passports
                .Where(p => required.All(r => p.ContainsKey(r)))
                .Count()
                .ToString();

            Console.WriteLine(PartA);

            var isValidPassports = passports
                .Select(p => (
                    p,
                    isValid:
                        required.All(r => p.ContainsKey(r))
                        && Enumerable.Range(1920, 2002-1920 + 1).Contains(Convert.ToInt32(p["byr"]))
                        && Enumerable.Range(2010, 2020-2010 + 1).Contains(Convert.ToInt32(p["iyr"]))
                        && Enumerable.Range(2020, 2030-2020 + 1).Contains(Convert.ToInt32(p["eyr"]))
                        && IsValidHeight(p["hgt"])
                        && Regex.IsMatch(p["hcl"], "^#[0-9a-f]{6}$")
                        && new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" }.Contains(p["ecl"])
                        && Regex.IsMatch(p["pid"], "^\\d{9}$")))
                .ToArray();

            var PartB = isValidPassports.Where(x => x.isValid).Count().ToString();

            Console.WriteLine(PartB);
        }

        private static bool IsValidHeight(string s) =>
                        s.Length >= 4 && s.Length<=5
                        && (s[^2..] switch
                        {
                            "in" => Enumerable.Range(59, 76 - 59 + 1).Contains(Convert.ToInt32(s[..^2])),
                            "cm" => Enumerable.Range(150, 193 - 150 + 1).Contains(Convert.ToInt32(s[..^2])),
                            _ => false,
                        });
    }
}
