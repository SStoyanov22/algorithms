﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace Day_24
{
    enum Neighbors
    {
        E, SE, SW, W, NW, NE
    }

    enum Colors
    {
        White, Black
    }

    class Tile
    {
        public Colors Color { get; set; }

        public void Flip()
        {
            Color = Color == Colors.White ? Colors.Black : Colors.White;
        }
    }
    class Program
    {
        //Using the Cube Coordinate for the hexagons
        private static readonly Dictionary<Neighbors, Vector3> directions = new()
        {
            { Neighbors.E, new Vector3(+1, -1, 0) },
            { Neighbors.SE, new Vector3(0, -1, +1) },
            { Neighbors.SW, new Vector3(-1, 0, +1) },
            { Neighbors.W, new Vector3(-1, +1, 0) },
            { Neighbors.NW, new Vector3(0, +1, -1) },
            { Neighbors.NE, new Vector3(+1, 0, -1) },
        };
        static void Main(string[] args)
        {
            var tiles = File.ReadAllLines(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day24\Day_24\input.txt")
                            .Select(line => GetTilePosition(line));
            var grid = new Dictionary<Vector3, Tile>();
            foreach (var tile in tiles)
            {
                if (grid.ContainsKey(tile))
                {
                    grid[tile].Flip();
                }
                else
                {
                    grid[tile] = new Tile { Color = Colors.Black };
                }
            }

            var answerPartOne = grid.Values.Where(t => t.Color == Colors.Black).Count();
            Console.WriteLine(answerPartOne);

            for (var i = 0; i < 100; i++)
            {
                grid = FlipTiles(grid);
            }

            var answerPartTwo = grid.Values.Where(t => t.Color == Colors.Black).Count();

            Console.WriteLine(answerPartTwo);
         }

        public static Dictionary<Vector3, Tile> FlipTiles(Dictionary<Vector3, Tile> grid)
        {
            var newGrid = new Dictionary<Vector3, Tile>();

            var max = grid.Keys
                .Select(k => new float[] { Math.Abs(k.X), Math.Abs(k.Y), Math.Abs(k.Z) })
                .SelectMany(x => x)
                .Max() + 1;

            for (var x = -max; x <= max; x++)
            {
                for (var y = -max; y <= max; y++)
                {
                    for (var z = -max; z <= max; z++)
                    {
                        if (x + y + z == 0)
                        {
                            var key = new Vector3(x, y, z);
                            if (!grid.ContainsKey(key))
                            {
                                grid[key] = new Tile();
                            }
                        }
                    }
                }
            }

            foreach (var tile in grid)
            {
                var adj = directions
                    .Select(d => grid.ContainsKey(tile.Key + d.Value) ?
                            grid[tile.Key + d.Value].Color : Colors.White)
                    .Where(c => c == Colors.Black)
                    .Count();

                if (tile.Value.Color == Colors.Black)
                {
                    newGrid[tile.Key] = adj == 0 || adj > 2
                        ? new Tile { Color = Colors.White }
                        : new Tile { Color = Colors.Black };
                }
                else if (tile.Value.Color == Colors.White)
                {
                    newGrid[tile.Key] = adj == 2
                        ? new Tile { Color = Colors.Black }
                        : new Tile { Color = Colors.White };
                }
            }

            return newGrid;
        }
        private static Vector3 GetTilePosition(string steps) =>
                            Regex.Matches(steps, @"e|se|sw|w|nw|ne")
                                 .Select(grp => Enum.Parse<Neighbors>(grp.Value.ToUpper()))
                                 .Select(n => directions[n])
                                 .Aggregate(new Vector3(0, 0, 0), (a, b) => a + b);
    }
}
