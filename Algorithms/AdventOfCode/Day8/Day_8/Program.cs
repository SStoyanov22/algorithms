﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_8
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllLines(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day8\Day_8\input.txt");
            var regex = new Regex(@"^(?<operation>\w+) (?<value>(\+|-)?\d+)$");
            var instructions = input.Select(l => regex.Match(l))
                .Select(m => 
				new Instruction( m.Groups["operation"].Value, Convert.ToInt32(m.Groups["value"].Value)))
                .ToArray();

			var PartA = RunProgram(instructions).acc.ToString();
            Console.WriteLine(PartA);
			for (int i = 0; i < instructions.Length; i++)
            {
                var orig = instructions[i];
                if (orig.Operation == "acc") continue;

                SwitchOperation(orig);

                var (looped, acc) = RunProgram(instructions);
                if (!looped)
                {
                    var PartB = acc.ToString();
                    Console.WriteLine(PartB);
                    break;
                }
                else
                    SwitchOperation(orig);
            }
        }

        private static void SwitchOperation(Instruction orig)
        {
            switch (orig.Operation)
            {
                case "nop":
                    orig.Operation = "jmp";
                    break;
                case "jmp":
                    orig.Operation = "nop";
                    break;
                default:
                    throw new InvalidOperationException();
            };
        }

        private static (bool looped, int acc) RunProgram(Instruction[] program)
		{
			var executedInstructions = new List<int>();
			int acc = 0, ip = 0;
			while (true)
			{
				var prog = program[ip];
				switch (prog.Operation)
				{
                    case "nop": ip++; break;

					case "acc":
						acc += prog.Value;
						ip++;
						break;

					case "jmp":
						ip += prog.Value;
						break;
				}

				if (ip >= program.Length)
					return (false, acc);
				else if (executedInstructions.Contains(ip))
					return (true, acc);
				else
					executedInstructions.Add(ip);
			}
		}


	}
}
