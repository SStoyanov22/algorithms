﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_8
{
     public class Instruction
    {
        public Instruction(string operation, int value)
        {
            this.Operation = operation;
            this.Value = value;
        }
        public string Operation { get; set; }
        public int Value { get; set; }
    }
}
