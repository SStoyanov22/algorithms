﻿
using System;
using System.Linq;

namespace Day_10
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllLines(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day10\Day_10\input.txt");

            var numbers = input
                .Select(x => int.Parse(x))
                .OrderBy(x => x)
                .ToArray();

			var differences = numbers
				.Prepend(0)
				.Append(numbers[^1] + 3)
				.Zip(numbers, (first, second) => new[] { first, second })
                .Select(x => x[1] - x[0])
                .ToArray();

            var counts = differences
                .GroupBy(x => x, (d, _) => (diff: d, count: _.Count()))
                .ToArray();

            var num1 = counts.Single(x => x.diff == 1).count;
            var num3 = counts.Single(x => x.diff == 3).count;

            var PartA = (num1 * num3).ToString();
            Console.WriteLine(PartA);

            var sequences = differences
                .Segment((cur, prev, _) => cur != prev)
                .Where(x => x.First() == 1)
                .Select(x => x.Count() switch
                {
                    1 => 1,
                    2 => 2,
                    3 => 4,
                    4 => 7,
                    5 => 15,
                    _ => throw new NotImplementedException("??"),
                })
                .Aggregate(1L, (agg, x) => agg * x);

            var PartB = sequences.ToString();
            Console.WriteLine(PartB);

        }
    }
}
