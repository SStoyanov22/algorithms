﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File
               .ReadAllLines(@"C:\My stuff\gitlab\algorithms\Algorithms\AdventOfCode\Day21\Day_21\input.txt");
            var regex = new Regex(@"^((?<ingredient>\w+) )+\(contains ((?<allergen>\w+)(, )?)+\)$", RegexOptions.ExplicitCapture);
            
            var recipes = input
                .Select(l => regex.Match(l))
                .Select(m => (
                    ingredients: m.Groups["ingredient"].Captures.Select(c => c.Value).ToList(),
                    allergens: m.Groups["allergen"].Captures.Select(c => c.Value).ToList()))
                .ToList();
            var allergenMap = new Dictionary<string, string>();
            var allergens = recipes.SelectMany(r => r.allergens).Distinct().ToList();

            while (allergens.Count!=0)
            {
                foreach (var allergen in allergens.ToList())
                {
                    var candidates = recipes.First(r => r.allergens.Contains(allergen)).ingredients.ToHashSet();
                    recipes.Where(r => r.allergens.Contains(allergen))
                        .Skip(1)
                        .ToList()
                        .ForEach(x => candidates.IntersectWith(x.ingredients));
                    if (candidates.Count == 1)
                    {
                        var i = allergenMap[allergen] = candidates.Single();
                        recipes.ForEach(r => r.ingredients.Remove(i));
                        allergens.Remove(allergen);
                    }
                }
            }

            var answerPartOne = recipes
                .SelectMany(r => r.ingredients)
                .Count()
                .ToString();
            Console.WriteLine(answerPartOne);

            var answerPartTwo = string.Join(",", allergenMap.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Value));
            Console.WriteLine(answerPartTwo);
        }
    }
}
