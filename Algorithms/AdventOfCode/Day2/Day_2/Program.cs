﻿using System;
using System.Collections.Generic;

namespace Day_2
{
    class Program
    {
        /*
         Your flight departs in a few days from the coastal airport; the easiest way down to the coast from here is via toboggan.

        The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day. "Something's wrong with our computers; we can't log in!" You ask if you can take a look.

        Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.

        To try to debug the problem, they have created a list (your puzzle input) of passwords (according to the corrupted database) and the corporate policy when that password was set.

        For example, suppose you have the following list:

        1-3 a: abcde
        1-3 b: cdefg
        2-9 c: ccccccccc
        Each line gives the password policy and then the password. The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password must contain a at least 1 time and at most 3 times.

        In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b, but needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits of their respective policies.

        How many passwords are valid according to their policies?
         */
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\stoyan.stoyanov\source\repos\gitlab\algorithms\Algorithms\AdventOfCode\Day2\Day_2\input.txt");
            var listOfPasswords = new List<string>();
            int countValid = 0;
            int countValid2 = 0;
            foreach (string line in lines)
            {
                var inputs = line.Split(' ');
                var ranges = inputs[0].Split('-');
                var lowBound = int.Parse(ranges[0]);
                var highBound = int.Parse(ranges[1]);
                var letter = inputs[1][0];
                var password = inputs[2];
                var count = CountLetters(password, letter);
                if (count >= lowBound && count <= highBound)
                {
                    countValid++;
                }
                if (CheckLetterPositions(lowBound, highBound, letter, password))
                {
                    countValid2++;
                }
            }
            Console.WriteLine(countValid);
            Console.WriteLine(countValid2);
        }
        public static int CountLetters(string word, char countableLetter)
        {
            int count = 0;
            foreach (char c in word)
            {
                if (countableLetter == c)
                    count++;
            }
            return count;
        }

        public static bool CheckLetterPositions(int firstPosition, int secondPosition, char letter, string password)
        {
            if (password[firstPosition-1] == letter
                ^ password[secondPosition -1] == letter)
            {
                return true;
            }

            return false;
        }
    }
}
