﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText(@"D:\gitlab\algorithms\Algorithms\AdventOfCode\Day22\Day_22\input.txt")
                .Split(new string[] { "\r\n\r\n" },
                               StringSplitOptions.RemoveEmptyEntries);
            var player1 = new Queue<int>(input.First()
                                              .Split(new string[] { "\r\n" },StringSplitOptions.RemoveEmptyEntries)
                                              .Skip(1)
                                              .Select(int.Parse)
                                              .ToArray());
            var player2 = new Queue<int>(input.Last()
                                              .Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                                              .Skip(1)
                                              .Select(int.Parse)
                                              .ToArray());
            var winner = PlayCombatGame(player1, player2);

            Console.WriteLine(Enumerable.Range(1, winner.Count()).Zip(winner.Reverse(), (i, card) => i * card).Sum());
            var recursivePlayer1 = new Queue<int>(input.First()
                                             .Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                                             .Skip(1)
                                             .Select(int.Parse)
                                             .ToArray());
            var recursivePlayer2 = new Queue<int>(input.Last()
                                              .Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                                              .Skip(1)
                                              .Select(int.Parse)
                                              .ToArray());
            var recursiveWinner = PlayRecursiveCombatGame(recursivePlayer1, recursivePlayer2);
            Console.WriteLine(Enumerable.Range(1, recursiveWinner.Count()).Zip(recursiveWinner.Reverse(), (i, card) => i * card).Sum());
        }

        public static void PlayRound(Queue<int> deck1, Queue<int> deck2)
        {
            var card1 = deck1.Dequeue();
            var card2 = deck2.Dequeue();

            if (card1 > card2)
            {
                deck1.Enqueue(card1);
                deck1.Enqueue(card2);
            }
            else
            {
                deck2.Enqueue(card2);
                deck2.Enqueue(card1);
            }
        }

        public static Queue<int> PlayCombatGame(Queue<int> player1, Queue<int> player2)
        {
            while (player1.Any() && player2.Any())
            {
                PlayRound(player1, player2);
            }

            return player1.Any() ? player1 : player2;
        }

        public static void PlayRecursiveRound(Queue<int> deck1, Queue<int> deck2)
        {
            var card1 = deck1.Dequeue();
            var card2 = deck2.Dequeue();

            if (card1 <= deck1.Count() && card2 <= deck2.Count())
            {
                var subDeck1 = new Queue<int>(deck1.Take(card1));
                var subDeck2 = new Queue<int>(deck2.Take(card2));

                PlayRecursiveCombatGame(subDeck1, subDeck2);

                if (subDeck1.Any())
                {
                    deck1.Enqueue(card1);
                    deck1.Enqueue(card2);
                }
                else
                {
                    deck2.Enqueue(card2);
                    deck2.Enqueue(card1);
                }
            }
            else
            {
                if (card1 > card2)
                {
                    deck1.Enqueue(card1);
                    deck1.Enqueue(card2);
                }
                else
                {
                    deck2.Enqueue(card2);
                    deck2.Enqueue(card1);
                }
            }
        }

        public static Queue<int> PlayRecursiveCombatGame(Queue<int> player1, Queue<int> player2)
        {
            var prevGameStates = new HashSet<string>();

            while (player1.Any() && player2.Any())
            {
                if (!prevGameStates.Add($"{string.Join(",", player1)}-{string.Join(",", player2)}"))
                {
                    return player1;
                }

                PlayRecursiveRound(player1, player2);
            }

            return player1.Any() ? player1 : player2;
        }
    }
}
