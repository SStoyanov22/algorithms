﻿using System;
using System.Collections.Generic;

namespace _1._5.OneWay
{
    class Program
    {
        /*  
            There are three types of edits that can be performed on strings: insert a character,
            remove a character, or replace a character. Given two strings, write a function to check if they are
            one edit (or zero edits) away.
            EXAMPLE
            pale, ple -> true
            pales, pale -> true
            pale, bale -> true
            pale, bake -> false
        */
        static void Main(string[] args)
        {
            var str1 = Console.ReadLine();
            var str2 = Console.ReadLine();

            var isOneWay = IsOneWay(str1, str2);

            Console.WriteLine(isOneWay);
        }

        private static bool IsOneWay(string str1, string str2)
        {
            if (str1.Length - str2.Length > 1 || str2.Length - str2.Length < -1)
            {
                return false;
            }

            if (str1 == str2)
            {
                return true;
            }

            if (str1.Length ==  str2.Length)
            {
                return OneReplaceAway(str1, str2);
            }

            // insert and remove are basically the same operations
            if (str1.Length > str2.Length)
            {
                return OneInsertAway(str1, str2);
            }
            else
            {
                return OneInsertAway(str2, str1);
            }
        }

        private static bool OneInsertAway(string longerStr, string shorterStr)
        {
            var indexLongerStr = 0;
            var indexShorterStr = 0;

            while (indexShorterStr<shorterStr.Length && indexLongerStr <longerStr.Length)
            {
                if (shorterStr[indexShorterStr] == longerStr[indexLongerStr])
                {
                    indexShorterStr++;
                    indexLongerStr++;
                }
                else
                {
                    //if we already moved one index forward in the longer string then return false
                    if (indexLongerStr != indexShorterStr)
                    {
                        return false;
                    }
                    //if characters at the current index aren't equal check next index in the longer string
                    indexLongerStr++;
                }
            }

            return true;
        }

        private static bool OneReplaceAway(string str1, string str2)
        {
            var foundDiff = false;

            for (int i = 0; i < str1.Length; i++)
            {
                if (str1[i] != str2[i])
                {
                    //there should be difference at only 1 position
                    if (foundDiff)
                    {
                        return false;
                    }

                    foundDiff = true;
                }
            }

            return true;
        }
    }
}
