﻿using System;

namespace _1._7.RotateMatrix
{
    class Program
    {
        /*
           Given an image represented by an NxN matrix, where each pixel in the image is 4
           bytes, write a method to rotate the image by 90 degrees. Can you do this in place?
        */
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            string[][] matrix = new string[n][];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    var pixel = Convert.ToString(Console.Read());
                    matrix[i][ j] = pixel;
                }
            }

            Rotate(matrix);
            Print(matrix);
        }

        private static bool Rotate(string[][] matrix)
        {
            if (matrix.Length == 0 || matrix.Length != matrix[0].Length)
            {
                return false;
            }

            var n = matrix.Length;
            //number of layers is n/2
            //iterate over each layer starting from the outer one
            for (int layer = 0; layer < n/2; layer++)
            {
                int first = layer;
                int last = n - 1 - layer;
                for (int i = first ; i < last; i++)

                {
                    int offset = i - first;

                    //keep first cell to use it for last
                    var top = matrix[first][i];

                    //left -> top
                    matrix[first][i] = matrix[last - offset][first];

                    //bottom -> left
                    matrix[last - offset][first] = matrix[last][last - offset];

                    //right -> bottom
                    matrix[last][last - offset] = matrix[i][last];

                    //top -> right
                    matrix[i][last] = top;
                }
            }

            return true;
        }

        private static void Print(string[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(matrix[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
