﻿using System;

namespace _1._4.PalindromePermutation
{
    class Program
    {
        /*
           Given a string, write a function to check if it is a permutation of a palindrome.
            A palindrome is a word or phrase that is the same forwards and backwards. A permutation
            is a rearrangement of letters. The palindrome does not need to be limited to just dictionary words.
            EXAMPLE
            Input: Tact Coa
            Output: True (permutations: "taco cat", "atco eta", etc.)
         */
        static void Main(string[] args)
        {
            var input = Console.ReadLine().ToLower();
            Console.WriteLine(IsPalindromePermutation(input));
           
        }
        //get char position in alohabetical position if it is small letter else -1
        private static int GetCharNumber(char c)
        {
            int a = 'a';
            int z = 'z';
            int val = c;
            if (val >= a && val <= z)
            {
                return val - a;
            }

            return -1;
        }
        //count all letters and check if at the end if we have 0 or 1 letters with odd count
        private static bool IsPalindromePermutation(string input)
        {
            var charsCount = new char['z' - 'a' +1] ;
            int countOdd = 0;
            foreach (var letter in input)
            {
                int x = GetCharNumber(letter);  
                if (x != -1)
                {
                    charsCount[x]++;
                    if (charsCount[x] % 2 == 1)
                    {
                        countOdd++;
                    }
                    else
                    {
                        countOdd--;
                    }
                }
            }
            return countOdd<=1;
        }
    }

   
}
